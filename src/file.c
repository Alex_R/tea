#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <libgen.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "file.h"

#include "die.h"


static char *_file_get_name_from_path(const char *file_path) {
    /* _file_get_name_from_path
     * Get the file name component from the supplied file path.
     *
     * @ const char *file_path - The path to get the name from.
     *
     * # char * - A newly allocated buffer containing the file name.
     */

    char *file_path_copy, *file_name;

    /* basename(3) modifies the supplied path, so we need a temporary copy. */
    file_path_copy = strdup(file_path);
    if(file_path_copy == NULL) {
        die("_file_get_name_from_path('%s') failed: strdup() failed: %s", file_path, strerror(errno));
    }

    file_name = strdup(basename(file_path_copy));
    if(file_name == NULL) {
        die("_file_get_name_from_path('%s') failed: strdup() failed: %s", file_path, strerror(errno));
    }

    free(file_path_copy);

    return file_name;
}


static unsigned char *_file_load_data_from_path(const char *file_path, size_t *data_size) {
    /* _file_load_data_from_path
     * Load the data from the file at the supplied path.
     * If the file doesn't exist, a NULL data pointer is returned, and data_size is set to 0.
     *
     * @ const char *file_path - The path to the file to load the data from.
     * @ size_t *data_size     - A pointer to a size_t to store the returned data's size in.
     *
     * # unsigned char * - A newly allocated buffer containing the file data.
     */

    int fd;
    ssize_t ret;
    unsigned char *data;
    off_t file_size;
    size_t bytes_read;

    fd = open(file_path, O_RDONLY);
    if(fd == -1) {
        if(errno == ENOENT) {
            *data_size = 0;
            return NULL;
        }

        die("_file_load_data_from_path('%s') failed: open() failed: %s", file_path, strerror(errno));
    }


    file_size = lseek(fd, 0, SEEK_END);
    if(file_size == -1) {
        die("_file_load_data_from_path('%s') failed: lseek(%d, SEEK_END) failed: %s", file_path, fd, strerror(errno));
    }

    if(lseek(fd, 0, SEEK_SET) == -1) {
        die("_file_load_data_from_path('%s') failed: lseek(%d, SEEK_SET) failed: %s", file_path, fd, strerror(errno));
    }


    data = malloc(sizeof(unsigned char) * file_size);
    if(data == NULL) {
        die("_file_load_data_from_path('%s') failed: malloc(%ld) failed: %s", file_path,
                                                                              (sizeof(unsigned char) * file_size),
                                                                              strerror(errno));
    }


    bytes_read = 0;
    while(file_size > bytes_read) {
        ret = read(fd, &data[bytes_read], (file_size - bytes_read));
        if(ret == -1) {
            die("_file_load_data_from_path('%s') failed: read(%d, %p, %ld) failed: %s", file_path, fd,
                                                                                        &data[bytes_read],
                                                                                        (file_size - bytes_read),
                                                                                        strerror(errno));
        }

        if(ret == 0) {
            break;
        }

        bytes_read += ret;
    }

    /* We might have break'd due to an early EOF (file truncated between our lseek()ing it and read()ing it),
     * so the actual file data size is bytes_read, not file_size (practically they will almost always be the same).
     */
    *data_size = bytes_read;


    if(close(fd) == -1) {
        die("_file_load_data_from_path('%s') failed: close(%d) failed: %s", file_path, fd, strerror(errno));
    }

    return data;
}


struct file *file_create_from_path(const char *file_path) {
    /* file_create_from_path
     * Create a new file structure, loading data from the file at the supplied path.
     * If the file at the supplied path does not exist, the file structure data is created empty.
     *
     * @ const char *file_path - The path to the file to load the data from.
     *
     * # struct file * - The newly created file structure.
     */

    struct file *file;

    file = malloc(sizeof(struct file));
    if(file == NULL) {
        die("file_create_from_path('%s') failed: malloc(%ld) failed: %s", file_path, sizeof(struct file),
                                                                          strerror(errno));
    }

    file->name = _file_get_name_from_path(file_path);

    file->path = strdup(file_path);
    if(file->path == NULL) {
        die("file_create_from_path('%s') failed: strdup() failed: %s", file_path, strerror(errno));
    }


    file->data = _file_load_data_from_path(file->path, &file->data_size);

    file->cursor = 0;

    file->gap_size = 0;

    file->changed = false;

    return file;
}

void file_destroy(struct file *file) {
    /* file_destroy
     * Destroy the supplied file structure.
     *
     * @ struct file *file - The file structure to destroy.
     */

    free(file->name);
    free(file->path);

    free(file->data);

    free(file);
}

int file_write(struct file *file) {
    /* file_write
     * Write the supplied file structure to the file at the structure's path.
     *
     * @ struct file *file - The file structure to write.
     *
     * # int - 0 if the file was written successfully, -1 otherwise with errno set appropriately.
     */

    int fd;
    ssize_t ret, bytes_written, write_size;
    unsigned char *data_ptr;

    fd = open(file->path, (O_WRONLY | O_CREAT | O_TRUNC));
    if(fd == -1) {
        goto error;
    }

    bytes_written = 0;
    while((file->data_size - bytes_written) > 0) {
        if(bytes_written < file->cursor) {
            data_ptr = &file->data[bytes_written];
            write_size = (file->cursor - bytes_written);
        } else {
            data_ptr = &file->data[(file->gap_size + bytes_written)];
            write_size = (file->data_size - bytes_written);
        }

        ret = write(fd, data_ptr, write_size);
        if(ret == -1) {
            goto close_and_error;
        }

        bytes_written += ret;
    }

    if(close(fd) == -1) {
        goto error;
    }

    file->changed = false;

    return 0;

    close_and_error:
        close(fd);
    error:
        return -1;
}


static void _file_gap_grow(struct file *file) {
    /* _file_gap_grow
     * Grow the gap buffer in the supplied file structure, if appropriate.
     *
     * @ struct file *file - The file structure to grow the gap buffer of.
     */

    if(file->gap_size > 0) {
        return;
    }

    file->data = realloc(file->data, (sizeof(unsigned char) * (file->data_size + FILE_GAP_BLOCK_SIZE)));
    if(file->data == NULL) {
        die("_file_gap_grow() failed: realloc(%ld) failed: %s", (sizeof(unsigned char) *
                                                                (file->data_size + FILE_GAP_BLOCK_SIZE)),
                                                                strerror(errno));
    }

    /* Copy data after the cursor to after the new gap size (moving the end into the newly allocated region). */
    memcpy(&file->data[(file->cursor + FILE_GAP_BLOCK_SIZE)],
           &file->data[file->cursor],
           (file->data_size - file->cursor));

    file->gap_size = FILE_GAP_BLOCK_SIZE;
}


void file_cursor_set(struct file *file, size_t cursor) {
    /* file_cursor_set
     * Set the supplied file structure's cursor position to the supplied value.
     *
     * @ struct file *file - The file structure to set the cursor position of.
     * @ size_t cursor     - The value to set the cursor position to.
     */

    if(file->cursor == cursor) {
        return;
    }

    if(cursor > file->cursor) {
        /* Copy data after the gap buffer into the start of the gap buffer (note that this may alias). */
        memmove(&file->data[file->cursor],
                &file->data[(file->cursor + file->gap_size)],
                (cursor - file->cursor));
    } else {
        /* Copy data before the cursor into the end of the gap buffer (note that this may alias). */
        memmove(&file->data[(cursor + file->gap_size)],
                &file->data[cursor],
                (file->cursor - cursor));
    }

    file->cursor = cursor;
}

size_t file_cursor_get(struct file *file) {
    /* file_cursor_get
     * Get the supplied file structure's cursor position.
     *
     * @ struct file *file - The file structure to get the cursor position of.
     *
     * # size_t - The file structure's cursor position.
     */

    return file->cursor;
}


void file_byte_add(struct file *file, unsigned char byte) {
    /* file_byte_add
     * Add the supplied byte to the supplied file structure at the file's current cursor position.
     *
     * @ struct file *file  - The file structure to add the byte to.
     * @ unsigned char byte - The byte to add.
     */

    _file_gap_grow(file);

    file->data[file->cursor] = byte;
    file->cursor++;
    file->data_size++;
    file->gap_size--;

    file->changed = true;
}

void file_byte_del(struct file *file) {
    /* file_byte_del
     * Delete a byte from the supplied file structureu after the file's current cursor position.
     *
     * @ struct file *file - The file structure to delete the byte from.
     */

    file->data_size--;
    file->gap_size++;

    file->changed = true;
}

unsigned char file_byte_get(struct file *file, size_t position) {
    /* file_byte_get
     * Get a byte from the supplied file structure's data at the supplied position.
     *
     * @ struct file *file - The file structure to get the byte from.
     * @ size_t position   - The position within the file's data to get the byte from.
     *
     * # unsigned char - The byte from the file's data.
     */

    if(position < file->cursor) {
        return file->data[position];
    }

    return file->data[file->gap_size + position];
}


size_t file_data_size_get(struct file *file) {
    /* file_data_size_get
     * Get the supplied file structure's data size.
     *
     * @ struct file *file - The file structure to get the data size of.
     *
     * # size_t - The file structure's data size.
     */

    return file->data_size;
}


bool file_changed(struct file *file) {
    /* file_changed
     * Check if the file has been changed since it was last loaded/saved.
     *
     * # bool - True if the file has been changed, False otherwise.
     */

    return file->changed;
}
