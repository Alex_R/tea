#ifndef EDITOR_ACTION_H_INCLUDED
#define EDITOR_ACTION_H_INCLUDED

#include "editor.h"


/* Function prototypes. */
size_t editor_action_previous_char(struct editor_context *context, size_t start_position, unsigned char character);
size_t editor_action_next_char(struct editor_context *context, size_t start_position, unsigned char character);
size_t editor_action_cursor_forward(struct editor_context *context, size_t cursor);
size_t editor_action_cursor_backward(struct editor_context *context, size_t cursor);
size_t editor_action_line_start(struct editor_context *context, size_t cursor);
size_t editor_action_line_end(struct editor_context *context, size_t cursor);
size_t editor_action_line_up(struct editor_context *context, size_t cursor);
size_t editor_action_line_down(struct editor_context *context, size_t cursor);

void editor_action_cursor_home(struct editor_context *context, input_t input);
void editor_action_cursor_end(struct editor_context *context, input_t input);
void editor_action_cursor_down(struct editor_context *context, input_t input);
void editor_action_cursor_up(struct editor_context *context, input_t input);
void editor_action_cursor_right(struct editor_context *context, input_t input);
void editor_action_cursor_left(struct editor_context *context, input_t input);
void editor_action_cursor_down_page(struct editor_context *context, input_t input);
void editor_action_cursor_up_page(struct editor_context *context, input_t input);

void editor_action_edit_add_char(struct editor_context *context, input_t input);
void editor_action_edit_add_newline(struct editor_context *context, input_t input);
void editor_action_edit_add_tab(struct editor_context *context, input_t input);

void editor_action_edit_del_char(struct editor_context *context, input_t input);
void editor_action_edit_del_char_before(struct editor_context *context, input_t input);

void editor_action_exit(struct editor_context *context, input_t input);
void editor_action_save(struct editor_context *context, input_t input);

#endif
