#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>

#include "config.h"

#include "die.h"
#include "terminal.h"


/* Global structure initialisation.
 * These are the default config values.
 */
struct config_context _config_context = {
    .expand_tab = true,
    .tab_size   = 4,
};


bool config_expand_tab_get(void) {
    return _config_context.expand_tab;
}

void config_expand_tab_set(bool value) {
    _config_context.expand_tab = value;
}


int config_tab_size_get(void) {
    return _config_context.tab_size;
}

void config_tab_size_set(int value) {
    if(value < 0) {
        die("config_tab_size_set(%d) failed: Value is less than 0", value);
    }

    _config_context.tab_size = value;

    /* We don't control the tab size during drawing, so we need to explicitly set it for curses. */
    terminal_tab_size_set(value);
}


static void _config_module_cleanup(void) {
    /* _config_module_cleanup
     * Cleanup the config module.
     * Set as an exit callback by atexit() in config_module_init().
     */
}

void config_module_init(void) {
    /* config_module_init
     * Initialise the config module.
     */

    /* TODO: Load config from file. */

    if(atexit(_config_module_cleanup) != 0) {
        die("config_moudle_init() failed: atexit() failed: %s", strerror(errno));
    }
}
