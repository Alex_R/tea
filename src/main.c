#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <getopt.h>
#include <locale.h>

#include "main.h"

#include "die.h"
#include "config.h"
#include "terminal.h"
#include "input.h"
#include "editor.h"


static void _main_args_display_usage(void) {
    /* _main_args_display_usage
     * Display the program usage information.
     */

    printf(MAIN_NAME " " MAIN_VERSION " - A text editor.\n"
           "Usage: "MAIN_NAME" [options] file\n"
           "Try '"MAIN_NAME" --help' for more information.\n"
          );
}

static void _main_args_display_help(void) {
    /* _main_args_display_help
     * Display the program help information.
     */

    printf(MAIN_NAME " " MAIN_VERSION " - A text editor.\n"
           "Copyright (c) Alex Richman <alex@richman.io>.\n"
           "Released as free and open-source software under the ISC licence.\n"
           "\n"
           "View the source and report bugs at <https://gitlab.com/Alex_R/tea>.\n"
           "\n"
           "Usage:\n"
           "  tea [options] file - Edit the specified file.\n"
           "\n"
           "Options:\n"
           "  -h, --help    - Display this help information.\n"
           "  -u, --usage   - Display simple usage information.\n"
           "  -v, --version - Display simple version information.\n"
          );
}

static void _main_args_display_version(void) {
    /* _main_args_display_version
     * DIsplay the program version information.
     */

    printf(MAIN_NAME " " MAIN_VERSION "\n");
}

static int _main_args_parse_opt(int key, const char *arg, struct main_args *arguments) {
    /* _main_args_parse_opt
     * Parse an option with the supplied key code and option index.
     *
     * @ int key                     - The key code for the option to parse, or a special MAIN_ARGS_* value.
     * @ char *arg                   - The argument for the option, if appropriate.
     * @ struct main_args *arguments - The arguments structure to update.
     *
     * # int - 0 on success, 1 to stop parsing, -1 on error.
     */

    switch(key) {
        case 'h':
            _main_args_display_help();
            return 1;

        case 'u':
            _main_args_display_usage();
            return 1;

        case 'v':
            _main_args_display_version();
            return 1;

        case MAIN_ARGS_EXTRA:
            if(arguments->file == NULL) {
                arguments->file = arg;
            }

            return 0;

        case MAIN_ARGS_END:
            if(arguments->file == NULL) {
                _main_args_display_usage();
                return -1;
            }

            return 0;

        default:
            _main_args_display_usage();
            return -1;
    };
}


static void _main_init_modules(struct main_args *arguments) {
    /* _main_init_modules
     * Initialise all modules.
     */

    if(setlocale(LC_ALL, "") == NULL) {
        die("_main_init_modules() failed: setlocale() failed");
    }

    config_module_init();

    terminal_module_init();

    input_module_init();

    editor_module_init(arguments->file);
}


int main(int argc, char **argv) {
    struct option options[] = {
        {"help",    no_argument, NULL, 'h'},
        {"usage",   no_argument, NULL, 'u'},
        {"version", no_argument, NULL, 'v'},

        {NULL, 0, NULL, 0},
    };
    int key, ret, iter;
    struct main_args arguments;

    memset(&arguments, 0, sizeof(struct main_args));

    /* Parse structured options. */
    while(1) {
        key = getopt_long(argc, argv, "huv", options, NULL);
        if(key == -1) {
            break;
        }

        ret = _main_args_parse_opt(key, optarg, &arguments);
        if(ret == -1) {
            return EXIT_FAILURE;
        }
        if(ret == 1) {
            return EXIT_SUCCESS;
        }
    }

    /* Parse extra arguments. */
    for(iter = optind; iter < argc; iter++) {
        ret = _main_args_parse_opt(MAIN_ARGS_EXTRA, argv[iter], &arguments);
        if(ret == -1) {
            return EXIT_FAILURE;
        }
        if(ret == 1) {
            return EXIT_SUCCESS;
        }
    }

    /* End argument parsing. */
    ret = _main_args_parse_opt(MAIN_ARGS_END, NULL, &arguments);
    if(ret == -1) {
        return EXIT_FAILURE;
    }
    if(ret == 1) {
        return EXIT_SUCCESS;
    }

    _main_init_modules(&arguments);
    editor_main();

    return EXIT_SUCCESS;
}
