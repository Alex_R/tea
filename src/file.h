#ifndef FILE_H_INCLUDED
#define FILE_H_INCLUDED

#include <stdbool.h>


/* Constants. */
#define FILE_GAP_BLOCK_SIZE 8192


/* Type definitions. */
struct file {
    char *name;
    char *path;

    unsigned char *data;
    size_t data_size, cursor, gap_size;

    bool changed;
};


/* Function prototypes. */
struct file *file_create_from_path(const char *file_path);
void file_destroy(struct file *file);

int file_write(struct file *file);

void file_cursor_set(struct file *file, size_t cursor);
size_t file_cursor_get(struct file *file);

void file_byte_add(struct file *file, unsigned char byte);
void file_byte_del(struct file *file);
unsigned char file_byte_get(struct file *file, size_t position);

size_t file_data_size_get(struct file *file);

bool file_changed(struct file *file);

#endif
