#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#include "editor.h"

#include "die.h"
#include "file.h"
#include "terminal.h"
#include "input.h"
#include "intmap.h"
#include "draw.h"
#include "editor_action.h"


/* Global structure initialisation. */
static struct editor_context _editor_context = {
    .file = NULL,
    .draw_context = NULL,
    .editor_action_map = NULL,
};


/* Editor action map definitions. */
static const struct intmap_mapping _editor_action_map_mappings[] = {
    {INPUT_ARROW_RIGHT, (intmap_int_t) &editor_action_cursor_right},
    {INPUT_ARROW_LEFT,  (intmap_int_t) &editor_action_cursor_left},
    {INPUT_ARROW_DOWN,  (intmap_int_t) &editor_action_cursor_down},
    {INPUT_ARROW_UP,    (intmap_int_t) &editor_action_cursor_up},

    {INPUT_HOME, (intmap_int_t) &editor_action_cursor_home},
    {INPUT_END,  (intmap_int_t) &editor_action_cursor_end},

    {INPUT_PAGE_NEXT,     (intmap_int_t) &editor_action_cursor_down_page},
    {INPUT_PAGE_PREVIOUS, (intmap_int_t) &editor_action_cursor_up_page},

    {' ',  (intmap_int_t) &editor_action_edit_add_char},
    {'!',  (intmap_int_t) &editor_action_edit_add_char},
    {'"',  (intmap_int_t) &editor_action_edit_add_char},
    {'#',  (intmap_int_t) &editor_action_edit_add_char},
    {'$',  (intmap_int_t) &editor_action_edit_add_char},
    {'%',  (intmap_int_t) &editor_action_edit_add_char},
    {'&',  (intmap_int_t) &editor_action_edit_add_char},
    {'\'', (intmap_int_t) &editor_action_edit_add_char},
    {'(',  (intmap_int_t) &editor_action_edit_add_char},
    {')',  (intmap_int_t) &editor_action_edit_add_char},
    {'*',  (intmap_int_t) &editor_action_edit_add_char},
    {'+',  (intmap_int_t) &editor_action_edit_add_char},
    {',',  (intmap_int_t) &editor_action_edit_add_char},
    {'-',  (intmap_int_t) &editor_action_edit_add_char},
    {'.',  (intmap_int_t) &editor_action_edit_add_char},
    {'/',  (intmap_int_t) &editor_action_edit_add_char},
    {'0',  (intmap_int_t) &editor_action_edit_add_char},
    {'1',  (intmap_int_t) &editor_action_edit_add_char},
    {'2',  (intmap_int_t) &editor_action_edit_add_char},
    {'3',  (intmap_int_t) &editor_action_edit_add_char},
    {'4',  (intmap_int_t) &editor_action_edit_add_char},
    {'5',  (intmap_int_t) &editor_action_edit_add_char},
    {'6',  (intmap_int_t) &editor_action_edit_add_char},
    {'7',  (intmap_int_t) &editor_action_edit_add_char},
    {'8',  (intmap_int_t) &editor_action_edit_add_char},
    {'9',  (intmap_int_t) &editor_action_edit_add_char},
    {':',  (intmap_int_t) &editor_action_edit_add_char},
    {';',  (intmap_int_t) &editor_action_edit_add_char},
    {'<',  (intmap_int_t) &editor_action_edit_add_char},
    {'=',  (intmap_int_t) &editor_action_edit_add_char},
    {'>',  (intmap_int_t) &editor_action_edit_add_char},
    {'?',  (intmap_int_t) &editor_action_edit_add_char},
    {'@',  (intmap_int_t) &editor_action_edit_add_char},
    {'A',  (intmap_int_t) &editor_action_edit_add_char},
    {'B',  (intmap_int_t) &editor_action_edit_add_char},
    {'C',  (intmap_int_t) &editor_action_edit_add_char},
    {'D',  (intmap_int_t) &editor_action_edit_add_char},
    {'E',  (intmap_int_t) &editor_action_edit_add_char},
    {'F',  (intmap_int_t) &editor_action_edit_add_char},
    {'G',  (intmap_int_t) &editor_action_edit_add_char},
    {'H',  (intmap_int_t) &editor_action_edit_add_char},
    {'I',  (intmap_int_t) &editor_action_edit_add_char},
    {'J',  (intmap_int_t) &editor_action_edit_add_char},
    {'K',  (intmap_int_t) &editor_action_edit_add_char},
    {'L',  (intmap_int_t) &editor_action_edit_add_char},
    {'M',  (intmap_int_t) &editor_action_edit_add_char},
    {'N',  (intmap_int_t) &editor_action_edit_add_char},
    {'O',  (intmap_int_t) &editor_action_edit_add_char},
    {'P',  (intmap_int_t) &editor_action_edit_add_char},
    {'Q',  (intmap_int_t) &editor_action_edit_add_char},
    {'R',  (intmap_int_t) &editor_action_edit_add_char},
    {'S',  (intmap_int_t) &editor_action_edit_add_char},
    {'T',  (intmap_int_t) &editor_action_edit_add_char},
    {'U',  (intmap_int_t) &editor_action_edit_add_char},
    {'V',  (intmap_int_t) &editor_action_edit_add_char},
    {'W',  (intmap_int_t) &editor_action_edit_add_char},
    {'X',  (intmap_int_t) &editor_action_edit_add_char},
    {'Y',  (intmap_int_t) &editor_action_edit_add_char},
    {'Z',  (intmap_int_t) &editor_action_edit_add_char},
    {'[',  (intmap_int_t) &editor_action_edit_add_char},
    {'\\', (intmap_int_t) &editor_action_edit_add_char},
    {']',  (intmap_int_t) &editor_action_edit_add_char},
    {'^',  (intmap_int_t) &editor_action_edit_add_char},
    {'_',  (intmap_int_t) &editor_action_edit_add_char},
    {'`',  (intmap_int_t) &editor_action_edit_add_char},
    {'a',  (intmap_int_t) &editor_action_edit_add_char},
    {'b',  (intmap_int_t) &editor_action_edit_add_char},
    {'c',  (intmap_int_t) &editor_action_edit_add_char},
    {'d',  (intmap_int_t) &editor_action_edit_add_char},
    {'e',  (intmap_int_t) &editor_action_edit_add_char},
    {'f',  (intmap_int_t) &editor_action_edit_add_char},
    {'g',  (intmap_int_t) &editor_action_edit_add_char},
    {'h',  (intmap_int_t) &editor_action_edit_add_char},
    {'i',  (intmap_int_t) &editor_action_edit_add_char},
    {'j',  (intmap_int_t) &editor_action_edit_add_char},
    {'k',  (intmap_int_t) &editor_action_edit_add_char},
    {'l',  (intmap_int_t) &editor_action_edit_add_char},
    {'m',  (intmap_int_t) &editor_action_edit_add_char},
    {'n',  (intmap_int_t) &editor_action_edit_add_char},
    {'o',  (intmap_int_t) &editor_action_edit_add_char},
    {'p',  (intmap_int_t) &editor_action_edit_add_char},
    {'q',  (intmap_int_t) &editor_action_edit_add_char},
    {'r',  (intmap_int_t) &editor_action_edit_add_char},
    {'s',  (intmap_int_t) &editor_action_edit_add_char},
    {'t',  (intmap_int_t) &editor_action_edit_add_char},
    {'u',  (intmap_int_t) &editor_action_edit_add_char},
    {'v',  (intmap_int_t) &editor_action_edit_add_char},
    {'w',  (intmap_int_t) &editor_action_edit_add_char},
    {'x',  (intmap_int_t) &editor_action_edit_add_char},
    {'y',  (intmap_int_t) &editor_action_edit_add_char},
    {'z',  (intmap_int_t) &editor_action_edit_add_char},
    {'{',  (intmap_int_t) &editor_action_edit_add_char},
    {'|',  (intmap_int_t) &editor_action_edit_add_char},
    {'}',  (intmap_int_t) &editor_action_edit_add_char},
    {'~',  (intmap_int_t) &editor_action_edit_add_char},

    {INPUT_RETURN, (intmap_int_t) &editor_action_edit_add_newline},

    {INPUT_TAB, (intmap_int_t) &editor_action_edit_add_tab},

    {INPUT_DELETE,    (intmap_int_t) &editor_action_edit_del_char},
    {INPUT_BACKSPACE, (intmap_int_t) &editor_action_edit_del_char_before},


    {INPUT_CONTROL_Q, (intmap_int_t) &editor_action_exit},
    {INPUT_CONTROL_S, (intmap_int_t) &editor_action_save},

    {INTMAP_MAP_END, INTMAP_MAP_END},
};


static void _editor_module_cleanup(void) {
    /* _editor_module_cleanup
     * Cleanup the editor module.
     * Set as an exit callback by atexit() in editor_module_init().
     */

    free(_editor_context.message.content);

    intmap_destroy(_editor_context.editor_action_map);

    draw_context_destroy(_editor_context.draw_context);

    file_destroy(_editor_context.file);
}


void editor_module_init(const char *file_path) {
    /* editor_module_init
     * Initialise the editor module.
     *
     * @ const char *file_path - The path to the file to edit.
     */

    _editor_context.file = file_create_from_path(file_path);

    _editor_context.draw_context = draw_context_create();

    _editor_context.editor_action_map = intmap_create(_editor_action_map_mappings);

    _editor_context.message.type = 0;
    _editor_context.message.content = NULL;

    if(atexit(_editor_module_cleanup) != 0) {
        die("editor_module_init() failed: atexit() failed: %s", strerror(errno));
    }
}


static void _editor_message_set(int type, const char *format, va_list vargs) {
    /* _editor_message_set
     * Set the editor's message with the supplied type and content.
     *
     * @ int type           - The type of the message (see EDITOR_MESSAGE_* constants).
     * @ const char *format - The format of the content.
     * @ va_list vargs      - Extra variadic arguments to use with the format to make the content.
     */

    size_t content_max_length = terminal_get_width();
    char content[content_max_length];

    if(vsnprintf(content, content_max_length, format, vargs) < 1) {
        die("_editor_message_set(%d, '%s') failed: vsnprintf() failed: %s", type, format, strerror(errno));
    }

    _editor_context.message.type = type;

    free(_editor_context.message.content);
    _editor_context.message.content = strdup(content);
    if(_editor_context.message.content == NULL) {
        die("_editor_message_set(%d, '%s') failed: strdup() failed: %s", type, content, strerror(errno));
    }
}

void editor_message_info(const char *format, ...) {
    /* editor_message_info
     * Set the editor's message to the supplied content, with an info type.
     *
     * @ const char *format - The format of the content.
     * @ ...                - Extra variadic arguments to use with the format to make the content.
     */

    va_list vargs;

    va_start(vargs, format);
    _editor_message_set(EDITOR_MESSAGE_INFO, format, vargs);
    va_end(vargs);
}

void editor_message_success(const char *format, ...) {
    /* editor_message_success
     * Set the editor's message to the supplied content, with a success type.
     *
     * @ const char *format - The format of the content.
     * @ ...                - Extra variadic arguments to use with the format to make the content.
     */

    va_list vargs;

    va_start(vargs, format);
    _editor_message_set(EDITOR_MESSAGE_SUCCESS, format, vargs);
    va_end(vargs);
}

void editor_message_warn(const char *format, ...) {
    /* editor_message_warn
     * Set the editor's message to the supplied content, with a warn type.
     *
     * @ const char *format - The format of the content.
     * @ ...                - Extra variadic arguments to use with the format to make the content.
     */

    va_list vargs;

    va_start(vargs, format);
    _editor_message_set(EDITOR_MESSAGE_WARN, format, vargs);
    va_end(vargs);
}

void editor_message_error(const char *format, ...) {
    /* editor_message_error
     * Set the editor's message to the supplied content, with an error type.
     *
     * @ const char *format - The format of the content.
     * @ ...                - Extra variadic arguments to use with the format to make the content.
     */

    va_list vargs;

    va_start(vargs, format);
    _editor_message_set(EDITOR_MESSAGE_ERROR, format, vargs);
    va_end(vargs);
}


static bool _editor_confirm(int type, const char *format, va_list vargs) {
    /* _editor_confirm
     * Draw the editor message with the supplied type and content, then await confirmation from the user.
     *
     * @ int type           - The type of the message (see EDITOR_MESSAGE_* constants).
     * @ const char *format - The format of the content.
     * @ va_list vargs      - Extra variadic arguments to use with the format to make the content.
     *
     * # bool - True if the user confirmed the action, False otherwise.
     */

    input_t input;

    _editor_message_set(type, format, vargs);
    draw_editor_message(_editor_context.draw_context,
                        _editor_context.message.type, _editor_context.message.content);
    _editor_context.message.type = EDITOR_MESSAGE_NONE;

    while(1) {
        input = input_get();

        if(input == 'y' || input == 'Y') {
            return true;
        }

        if(input == 'n' || input == 'N') {
            return false;
        }
    }
}

bool editor_confirm_info(const char *format, ...) {
    /* editor_confirm_info
     * Set the editor's message to the supplied content, with an info type, then await confirmation from the user.
     *
     * @ const char *format - The format of the content.
     * @ ...                - Extra variadic arguments to use with the format to make the content.
     *
     * # bool - True if the user confirmed the action, False otherwise.
     */

    va_list vargs;
    bool confirmation;

    va_start(vargs, format);
    confirmation = _editor_confirm(EDITOR_MESSAGE_INFO, format, vargs);
    va_end(vargs);

    return confirmation;
}

bool editor_confirm_success(const char *format, ...) {
    /* editor_confirm_success
     * Set the editor's message to the supplied content, with a success type, then await confirmation from the user.
     *
     * @ const char *format - The format of the content.
     * @ ...                - Extra variadic arguments to use with the format to make the content.
     *
     * # bool - True if the user confirmed the action, False otherwise.
     */

    va_list vargs;
    bool confirmation;

    va_start(vargs, format);
    confirmation = _editor_confirm(EDITOR_MESSAGE_SUCCESS, format, vargs);
    va_end(vargs);

    return confirmation;
}

bool editor_confirm_warn(const char *format, ...) {
    /* editor_confirm_warn
     * Set the editor's message to the supplied content, with a warn type, then await confirmation from the user.
     *
     * @ const char *format - The format of the content.
     * @ ...                - Extra variadic arguments to use with the format to make the content.
     *
     * # bool - True if the user confirmed the action, False otherwise.
     */

    va_list vargs;
    bool confirmation;

    va_start(vargs, format);
    confirmation = _editor_confirm(EDITOR_MESSAGE_WARN, format, vargs);
    va_end(vargs);

    return confirmation;
}

bool editor_confirm_error(const char *format, ...) {
    /* editor_confirm_error
     * Set the editor's message to the supplied content, with an error type, then await confirmation from the user.
     *
     * @ const char *format - The format of the content.
     * @ ...                - Extra variadic arguments to use with the format to make the content.
     *
     * # bool - True if the user confirmed the action, False otherwise.
     */

    va_list vargs;
    bool confirmation;

    va_start(vargs, format);
    confirmation = _editor_confirm(EDITOR_MESSAGE_ERROR, format, vargs);
    va_end(vargs);

    return confirmation;
}


static void _editor_main_draw(void) {
    /* _editor_main_draw
     * Update the terminal to reflect the current editor state.
     */

    terminal_clear();
    draw_file(_editor_context.draw_context, _editor_context.file);

    if(_editor_context.message.type != EDITOR_MESSAGE_NONE) {
        draw_editor_message(_editor_context.draw_context,
                            _editor_context.message.type, _editor_context.message.content);
        _editor_context.message.type = EDITOR_MESSAGE_NONE;
    }

    terminal_refresh();
}

static void _editor_main_update(void) {
    /* _editor_main_update
     * Update the editor context.
     */

    size_t file_cursor, file_draw_start, file_draw_end;

    file_cursor = file_cursor_get(_editor_context.file);
    file_draw_start = draw_context_file_start_get(_editor_context.draw_context);
    file_draw_end = draw_context_file_end_get(_editor_context.draw_context);

    while(file_draw_start > file_cursor) {
        file_draw_start = editor_action_line_up(&_editor_context, file_draw_start);
    }

    while(file_draw_end < file_cursor) {
        file_draw_start = editor_action_line_down(&_editor_context, file_draw_start);
        file_draw_end = draw_file_recalculate_end(_editor_context.draw_context, _editor_context.file,
                                                                                file_draw_start);
    }

    draw_context_file_start_set(_editor_context.draw_context, file_draw_start);
}

static void _editor_main_handle_input(void) {
    /* _editor_main_handle_input
     * Get one character of user input and perform the actions associated with it.
     */

    input_t input;
    editor_action_t *action_function;

    input = input_get();

    action_function = (editor_action_t *) intmap_right_from_left(_editor_context.editor_action_map, input);
    if((input_t) action_function == (INTMAP_MAP_END)) {
        return;
    }

    action_function(&_editor_context, input);
}

void editor_main(void) {
    /* editor_main
     * The editor entry point.  Does not return.
     */

    _editor_main_draw();

    while(1) {
        _editor_main_handle_input();
        _editor_main_update();
        _editor_main_draw();
    }
}
