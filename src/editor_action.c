#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "editor_action.h"

#include "die.h"
#include "config.h"
#include "editor.h"
#include "file.h"
#include "terminal.h"


size_t editor_action_previous_char(struct editor_context *context, size_t start_position, unsigned char character) {
    size_t cursor;

    for(cursor = start_position; cursor > 0; cursor--) {
        if(file_byte_get(context->file, cursor) == character) {
            return cursor;
        }
    }

    if(file_byte_get(context->file, cursor) == character) {
        return cursor;
    }

    return ((size_t) -1);
}

size_t editor_action_next_char(struct editor_context *context, size_t start_position, unsigned char character) {
    size_t cursor;

    for(cursor = start_position; cursor < file_data_size_get(context->file); cursor++) {
        if(file_byte_get(context->file, cursor) == character) {
            return cursor;
        }
    }

    return ((size_t) -1);
}

size_t editor_action_cursor_forward(struct editor_context *context, size_t cursor) {
    if(cursor < file_data_size_get(context->file)) {
        return (cursor + 1);
    }

    return cursor;
}

size_t editor_action_cursor_backward(struct editor_context *context, size_t cursor) {
    if(cursor > 0) {
        return (cursor - 1);
    }

    return cursor;
}

size_t editor_action_line_start(struct editor_context *context, size_t cursor) {
    if(cursor == 0) {
        return 0;
    }

    if(file_byte_get(context->file, cursor) == '\n') {
        cursor = editor_action_cursor_backward(context, cursor);
    }

    cursor = editor_action_previous_char(context, cursor, '\n');
    if(cursor == ((size_t) -1)) {
        cursor = 0;
    } else {
        cursor = editor_action_cursor_forward(context, cursor);
    }

    return cursor;
}

size_t editor_action_line_end(struct editor_context *context, size_t cursor) {
    cursor = editor_action_next_char(context, cursor, '\n');
    if(cursor == ((size_t) -1)) {
        cursor = file_data_size_get(context->file);
    }

    return cursor;
}

size_t editor_action_line_up(struct editor_context *context, size_t cursor) {
    cursor = editor_action_line_start(context, cursor);
    cursor = editor_action_cursor_backward(context, cursor);
    cursor = editor_action_line_start(context, cursor);

    return cursor;
}

size_t editor_action_line_down(struct editor_context *context, size_t cursor) {
    size_t new_cursor;

    new_cursor = editor_action_line_end(context, cursor);
    if(new_cursor == file_data_size_get(context->file)) {
        return cursor;
    }

    new_cursor = editor_action_cursor_forward(context, new_cursor);

    return new_cursor;
}


void editor_action_cursor_right(struct editor_context *context, input_t input) {
    size_t cursor;

    cursor = file_cursor_get(context->file);

    if(file_byte_get(context->file, cursor) == '\n') {
        return;
    }

    cursor = editor_action_cursor_forward(context, cursor);
    file_cursor_set(context->file, cursor);
}

void editor_action_cursor_left(struct editor_context *context, input_t input) {
    size_t cursor;

    cursor = file_cursor_get(context->file);
    cursor = editor_action_cursor_backward(context, cursor);

    if(file_byte_get(context->file, cursor) == '\n') {
        return;
    }

    file_cursor_set(context->file, cursor);
}

void editor_action_cursor_home(struct editor_context *context, input_t input) {
    size_t cursor;

    cursor = file_cursor_get(context->file);
    cursor = editor_action_line_start(context, cursor);
    file_cursor_set(context->file, cursor);
}

void editor_action_cursor_end(struct editor_context *context, input_t input) {
    size_t cursor;

    cursor = file_cursor_get(context->file);
    cursor = editor_action_line_end(context, cursor);
    file_cursor_set(context->file, cursor);
}

void editor_action_cursor_down(struct editor_context *context, input_t input) {
    size_t cursor;

    cursor = file_cursor_get(context->file);
    cursor = editor_action_line_down(context, cursor);
    file_cursor_set(context->file, cursor);
}

void editor_action_cursor_up(struct editor_context *context, input_t input) {
    size_t cursor;

    cursor = file_cursor_get(context->file);
    cursor = editor_action_line_up(context, cursor);
    file_cursor_set(context->file, cursor);
}

void editor_action_cursor_down_page(struct editor_context *context, input_t input) {
    int iter;
    size_t cursor;

    cursor = file_cursor_get(context->file);

    for(iter = 0; iter < (terminal_get_height() - 1); iter++) {
        cursor = editor_action_line_down(context, cursor);
    }

    file_cursor_set(context->file, cursor);
}

void editor_action_cursor_up_page(struct editor_context *context, input_t input) {
    int iter;
    size_t cursor;

    cursor = file_cursor_get(context->file);

    for(iter = 0; iter < (terminal_get_height() - 1); iter++) {
        cursor = editor_action_line_up(context, cursor);
    }

    file_cursor_set(context->file, cursor);
}


void editor_action_edit_add_char(struct editor_context *context, input_t input) {
    file_byte_add(context->file, (unsigned char) input);
}

void editor_action_edit_add_newline(struct editor_context *context, input_t input) {
    editor_action_edit_add_char(context, '\n');
}

void editor_action_edit_add_tab(struct editor_context *context, input_t input) {
    int iter;

    if(config_expand_tab_get()) {
        for(iter = 0; iter < config_tab_size_get(); iter++) {
            editor_action_edit_add_char(context, ' ');
        }
    } else {
        editor_action_edit_add_char(context, '\t');
    }
}


void editor_action_edit_del_char(struct editor_context *context, input_t input) {
    if(file_data_size_get(context->file) > 0 && file_cursor_get(context->file) < file_data_size_get(context->file)) {
        file_byte_del(context->file);
    }
}

void editor_action_edit_del_char_before(struct editor_context *context, input_t input) {
    size_t cursor;

    cursor = file_cursor_get(context->file);
    if(cursor > 0) {
        file_cursor_set(context->file, (cursor - 1));
        editor_action_edit_del_char(context, input);
    }
}


void editor_action_exit(struct editor_context *context, input_t input) {
    if(file_changed(context->file)) {
        if(!editor_confirm_warn("File modified since last save - Exit anyway? [y/n]")) {
            return;
        }
    }

    exit(EXIT_SUCCESS);
}

void editor_action_save(struct editor_context *context, input_t input) {
    if(file_write(context->file) == -1) {
        editor_message_error("Failed to save file: %s", strerror(errno));
    } else {
        editor_message_success("File saved: '%s'", context->file->path);
    }
}
