#ifndef CONFIG_H_INCLUDED
#define CONFIG_H_INCLUDED

#include <stdbool.h>


/* Global structures. */
struct config_context {
    bool expand_tab; /* True if tabs should be expanded (into spaces), False otherwise. */
    int tab_size;    /* The number of spaces that each tab should be represented by. */
};


/* Function prototypes. */
void config_module_init(void);

bool config_expand_tab_get(void);
void config_expand_tab_set(bool value);

int config_tab_size_get(void);
void config_tab_size_set(int value);

#endif
