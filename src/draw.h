#ifndef DRAW_H_INCLUDED
#define DRAW_H_INCLUDED

#include "file.h"


/* Type definitions. */
struct draw_context {
    size_t file_start; /* The position with the file data to start drawing at (set by editor). */
    size_t file_end;   /* The position within the file at which file drawing ended (set by draw_file). */

    int file_cursor_x, file_cursor_y; /* The terminal position where the character under the file cursor is drawn
                                       * (set by draw_file).
                                       */
};


/* Function prototypes. */
struct draw_context *draw_context_create(void);
void draw_context_destroy(struct draw_context *context);

void draw_context_file_start_set(struct draw_context *context, size_t file_start);
size_t draw_context_file_start_get(struct draw_context *context);

size_t draw_context_file_end_get(struct draw_context *context);

void draw_file(struct draw_context *context, struct file *file);
size_t draw_file_recalculate_end(struct draw_context *context, struct file *file, size_t file_start);

void draw_editor_message(struct draw_context *context, int type, const char *content);

#endif
