#ifndef EDITOR_H_INCLUDED
#define EDITOR_H_INCLUDED

#include "file.h"
#include "draw.h"
#include "input.h"
#include "intmap.h"


/* Constants. */
#define EDITOR_MESSAGE_NONE    0
#define EDITOR_MESSAGE_INFO    1
#define EDITOR_MESSAGE_SUCCESS 2
#define EDITOR_MESSAGE_WARN    3
#define EDITOR_MESSAGE_ERROR   4


/* Global structures. */
struct editor_context {
    struct file *file;
    struct draw_context *draw_context;
    struct intmap *editor_action_map;

    struct {
        int type;
        char *content;
    } message;
};


/* Type definitions. */
typedef void editor_action_t(struct editor_context *context, input_t input);


/* Function prototypes. */
void editor_module_init(const char *file_path);
void editor_main(void);

void editor_message_info(const char *format, ...);
void editor_message_success(const char *format, ...);
void editor_message_warn(const char *format, ...);
void editor_message_error(const char *format, ...);

bool editor_confirm_info(const char *format, ...);
bool editor_confirm_success(const char *format, ...);
bool editor_confirm_warn(const char *format, ...);
bool editor_confirm_error(const char *format, ...);

#endif
