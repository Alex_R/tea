#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

/* Constants. */
#define MAIN_NAME    "tea"
#define MAIN_VERSION "v0.1"

#define MAIN_ARGS_END   -1
#define MAIN_ARGS_EXTRA -2


/* Type definitions. */
struct main_args {
    const char *file;
};


/* Function prototypes. */
int main(int argc, char **argv);

#endif
