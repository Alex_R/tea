#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "terminal.h"

#include "die.h"
#include "config.h"


/* Global structure initialisation. */
static struct terminal_context _terminal_context = {
    .window = NULL,
};


static void _terminal_module_cleanup(void) {
    /* _terminal_module_cleanup
     * Cleanup the terminal module.
     * Set as an exit callback by exit() in terminal_module_init().
     */

    /* Reset terminal settings (as set by initscr() and co in terminal_module_init()). */
    if(endwin() == ERR) {
        die("_terminal_module_cleanup() failed");
    }

    /* Destroy the main window. */
    if(delwin(_terminal_context.window) == ERR) {
        die("_terminal_module_cleanup() failed");
    }
}

void terminal_emergency_reset(void) {
    /* terminal_emergency_reset
     * Attempt to reset the terminal settings.
     * This function may be called in an emergency (by die()), so should not fail, and not call die() itself.
     */

    /* Hopefully reset the terminal settings.
     * The terminal settings may be fairly screwed, as we could have been called during the terminal initialisation,
     * but endwin() is safe to call regardless (the worst that can happen is that we get a partially reset terminal).
     */
    if(endwin() == ERR) {
        /* Do NOT call die() here, as we have just been called by die() and recursion isn't as fun as it sounds. */
        fprintf(stderr, "terminal_emergency_reset() failed: endwin() failed\n");
    }
}


static void _terminal_module_init_colour_pair(int pair_id, int foreground_colour) {
    /* _terminal_module_init_colour_pair
     * Initialise a new colour pair with the supplied pair_id and foreground colour.
     *
     * @ int pair_id           - The id of the pair to initialise,
     * @ int foreground_colour - The foreground colour for the new pair.
     */

    if(init_pair(pair_id, foreground_colour, COLOR_BLACK) == ERR) {
        die("_terminal_module_init_colour_pair(%d, %d) failed: init_pair() failed", pair_id, foreground_colour);
    }
}

static void _terminal_module_init_colour_pairs(void) {
    /* _terminal_module_init_colour_pairs
     * Initialise the terminal colour pairs.
     */

    _terminal_module_init_colour_pair(TERMINAL_COLOUR_WHITE,   COLOR_WHITE);
    _terminal_module_init_colour_pair(TERMINAL_COLOUR_RED,     COLOR_RED);
    _terminal_module_init_colour_pair(TERMINAL_COLOUR_GREEN,   COLOR_GREEN);
    _terminal_module_init_colour_pair(TERMINAL_COLOUR_YELLOW,  COLOR_YELLOW);
    _terminal_module_init_colour_pair(TERMINAL_COLOUR_BLUE,    COLOR_BLUE);
    _terminal_module_init_colour_pair(TERMINAL_COLOUR_MAGENTA, COLOR_MAGENTA);
    _terminal_module_init_colour_pair(TERMINAL_COLOUR_CYAN,    COLOR_CYAN);
}

void terminal_module_init(void) {
    /* terminal_module_init
     * Initialise the terminal module.
     * Beware, here be curses dragons.
     */

    /* Initialise the terminal and curses data structures. */
    _terminal_context.window = initscr();
    if(_terminal_context.window == NULL) {
        die("terminal_module_init() failed: initscr() failed");
    }

    if(start_color() == ERR) {
        die("terminal_module_init() failed: start_color() failed");
    }

    _terminal_module_init_colour_pairs();

    /* Disable newline translation. */
    if(nonl() == ERR) {
        die("terminal_module_init() failed: nonl() failed");
    }

    /* Place the terminal into raw mode (input is passed through directly rather than being buffered,
     * and CTRL+C etc are passed through rather than generating a signal).
     */
    if(raw() == ERR) {
        die("terminal_module_init() failed: raw() failed");
    }

    /* Disable echoing of characters typed by the user. */
    if(noecho() == ERR) {
        die("terminal_module_init() failed: noecho() failed");
    }

    /* Enable translation of special keys (function keys, arrow keys etc) into character inputs. */
    if(keypad(_terminal_context.window, TRUE) == ERR) {
        die("terminal_module_init() failed: keypad() failed");
    }

    if(meta(_terminal_context.window, TRUE) == ERR) {
        die("terminal_module_init() failed: meta() failed");
    }

    /* Set the terminal escape delay (time for collation of escape sequences) to 25 miliseconds. */
    ESCDELAY = 25;

    terminal_tab_size_set(config_tab_size_get());

    if(atexit(_terminal_module_cleanup) != 0) {
        die("terminal_module_init() failed: atexit() failed");
    }
}


void terminal_get_width_height(int *width, int *height) {
    /* terminal_get_width_height
     * Get the current width and height of the terminal.
     *
     * @ int *width  - A pointer to an int to store the current terminal width in.
     * @ int *height - A pointer to an int to store the current terminal height in.
     */

    /* Note that getmaxyx is a macro, and directly sets the value of y and x, which is why they're not pointed to. */
    getmaxyx(_terminal_context.window, *height, *width);
    if(*width == -1 || *height == -1) {
        die("terminal_get_width_height() failed: getmaxyx() failed");
    }
}

int terminal_get_width(void) {
    /* terminal_get_width
     * Get the current terminal width.
     *
     * # int - The width of the terminal.
     */

    int width, height;

    terminal_get_width_height(&width, &height);

    return width;
}

int terminal_get_height(void) {
    /* terminal_get_height
     * Get the current terminal height.
     *
     * # int - The height of the terminal.
     */

    int width, height;

    terminal_get_width_height(&width, &height);

    return height;
}


void terminal_tab_size_set(int tab_size) {
    /* terminal_tab_size_set
     * Set the terminal tab size (number of spaces drawn per tab) to the supplied value.
     *
     * @ int tab_size - The value to set the tab size to.
     */

    TABSIZE = tab_size;
}


void terminal_clear(void) {
    /* termnial_clear
     * Clear the terminal, erasing the contents and ensuring that the next refresh redraws the entire terminal.
     */

    if(werase(_terminal_context.window) == ERR) {
        die("terminal_clear() failed: werase(%p) failed", _terminal_context.window);
    }
}

void terminal_refresh(void) {
    /* terminal_refresh
     * Refresh the terminal, drawing any updates since the last refresh.
     */

    if(wrefresh(_terminal_context.window) == ERR) {
        die("terminal_refresh() failed: wrefresh(%p) failed", _terminal_context.window);
    }
}


void terminal_cursor_move(int x, int y) {
    /* terminal_cursor_move
     * Move the terminal cursor to the supplied coordinates, which must not be outside the terminal.
     *
     * @ int x - The x coordinate to move the cursor to.
     * @ int y - The y coordinate to move the cursor to.
     */

    /* Note: Curses uses (y, x), not (x, y) for the argument ordering. */
    if(wmove(_terminal_context.window, y, x) == ERR) {
        die("terminal_cursor_move(%d, %d) failed: wmove(%p) failed", x, y, _terminal_context.window);
    }
}


void terminal_draw_char(char character) {
    /* terminal_draw_char
     * Draw the supplied character to the terminal under the current cursor position.
     * After the draw the cursor is advanced, and wrapped if the edge of the terminal is reached.
     *
     * @ char character - The character to draw.
     */

    if(waddch(_terminal_context.window, character) == ERR) {
        die("terminal_draw_char(%d) failed: waddch(%p) failed", character, _terminal_context.window);
    }
}

void terminal_draw_char_nomove(char character) {
    /* terminal_draw_char_nomove
     * Draw the supplied character to the terminal under the current cursor position.
     * Unlike terminal_draw_char(), this function does not advance the cursor.
     *
     * @ char character - The character to draw.
     */

    if(winsch(_terminal_context.window, character) == ERR) {
        die("terminal_draw_char_nomove(%d) failed: winsch(%p) failed", character, _terminal_context.window);
    }
}


void terminal_draw_string(const char *string) {
    /* terminal_draw_string
     * Draw the supplied string of characters to the terminal under the current cursor position.
     * After the draw the cursor is advanced, and wrapped if the edge of the terminal is reached.
     *
     * @ const char *string - The string of characters to draw.
     */

    if(waddstr(_terminal_context.window, string) == ERR) {
        die("terminal_draw_string(%p) failed: waddstr(%p) failed", string, _terminal_context.window);
    }
}

void terminal_draw_string_nomove(const char *string) {
    /* terminal_draw_string_nomove
     * Draw the supplied string of characters to the terminal under the current cursor position.
     * Unlike terminal_draw_string(), this function does not advance the cursor.
     *
     * @ const char *string - The string of characters to draw.
     */

    if(winsstr(_terminal_context.window, string) == ERR) {
        die("terminal_draw_string_nomove(%p) failed: insstr(%p) failed", string, _terminal_context.window);
    }
}


void terminal_attr_set(int attr_id) {
    /* terminal_attr_set
     * Set the attribute with the supplied attribute id.
     * All draws after the set will have the attribute added.
     *
     * @ int attr_id - The ID of the attribute to set (see TERMINAL_ATTR_* constants).
     */

    if(wattr_on(_terminal_context.window, attr_id, NULL) == ERR) {
        die("terminal_attr_set(%d) failed: wattr_on() failed", attr_id);
    }
}

void terminal_attr_clear(int attr_id) {
    /* terminal_attr_clear
     * Clear the attribute with the supplied attribte id.
     * All draws after the clear will not have the attribute added.
     *
     * @ int attr_id - The ID of the attribute to clear (see TERMINAL_ATTR_* constants).
     */

    if(wattr_off(_terminal_context.window, attr_id, NULL) == ERR) {
        die("terminal_attr_clear(%d) failed: wattr_off() failed", attr_id);
    }
}


void terminal_colour_set(int colour_id) {
    /* terminal_colour_set
     * Set the colour with the supplied colour id.
     * All draws after the set will use the new colour.
     *
     * @ int colour_id - The ID of the colour to set.
     */

    if(wcolor_set(_terminal_context.window, colour_id, NULL) == ERR) {
        die("terminal_colour_set(%d) failed: wcolor_set() failed", colour_id);
    }
}


int terminal_get_input(void) {
    /* terminal_get_input
     * Get a single character of input from the terminal, blocking if none is currently available.
     *
     * # int - The input character, which may be a ncurses special key code.
     */

    int ret;

    while(1) {
        ret = wgetch(_terminal_context.window);
        if(ret == ERR) {
            continue;
        }

        return ret;
    }
}
