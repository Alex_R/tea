#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <signal.h>

#include "die.h"

#include "main.h"
#include "terminal.h"


static void _die_abort(void) {
    /* _die_abort
     * Unblock the SIGABRT signal, then raise it to abort the instance.
     */

    struct sigaction action;

    action.sa_handler = SIG_DFL;
    action.sa_flags = SA_SIGINFO;

    if(sigaction(SIGABRT, &action, NULL) == -1) {
        die("_die_abort() failed: sigaction() failed: %s", strerror(errno));
    }

    abort();
}

void die_internal(const char *file_name, int file_line, const char *format, ...) {
    /* die_internal
     * Write an error message to STDERR then abort the instance.
     * This function is internal, and should not be called directly.  Use the die() macro instead.
     * A newline is appended to the generated error message.
     *
     * @ const char *file_name - The name of the source file in which the error occured.  Supplied by the die() macro.
     * @ const char *file_line - The line in the source file in which the error occured.  Supplied by the die() macro.
     * @ const char *format    - The format of the error message to log (in the printf() style).
     * @ ...                   - Extra variadic arguments to use with the format to create the error message.
     */

    va_list vargs;

    /* Allocating the message buffer on the stack with a constant size is a little ugly, but because
     * die() is called in all sorts of dogy situations we want to avoid any heap memory allocation.
     * Besides, who's going to use a message longer than a few thousand characters? ;)
     */
    char message[DIE_MESSAGE_SIZE_MAX];
    size_t message_size;

    /* Attempt to reset the terminal to a state that allows us to log our error message. */
    terminal_emergency_reset();

    va_start(vargs, format);

    if(snprintf(message, DIE_MESSAGE_SIZE_MAX, "%s-%s @%s:%d: ", MAIN_NAME, MAIN_VERSION,
                                                                 file_name, file_line) < 0) {
        fprintf(stderr, "die_internal(%s, %d, %s) failed: snprintf() failed: %s\n", file_name, file_line, format,
                                                                                    strerror(errno));
        _die_abort();
    }

    message_size = strlen(message);
    if(vsnprintf(&message[message_size], (DIE_MESSAGE_SIZE_MAX - message_size), format, vargs) < 0) {
        fprintf(stderr, "die_internal(%s, %d, %s) failed: vsnprintf() failed: %s\n", file_name, file_line, format,
                                                                                     strerror(errno));
        _die_abort();
    }

    fprintf(stderr, "%s\n", message);

    va_end(vargs);

    _die_abort();
}
