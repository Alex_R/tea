#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>

#include "intmap.h"

#include "die.h"


static struct intmap_mapping *_intmap_mapping_create(intmap_int_t left_side, intmap_int_t right_side) {
    /* _intmap_mapping_create
     * Create a new intmap mapping with the supplied left and right side values.
     *
     * @ intmap_int_t left_side  - The left side value of the mapping.
     * @ intmap_int_t right_side - The right side value of the mapping.
     *
     * # struct intmap_mapping * - The newly created mapping.
     */

    struct intmap_mapping *mapping;

    mapping = malloc(sizeof(struct intmap_mapping));
    if(mapping == NULL) {
        die("_intmap_mapping_create() failed: malloc(%ld) failed: %s", sizeof(struct intmap_mapping),
                                                                       strerror(errno));
    }

    mapping->left_side = left_side;
    mapping->right_side = right_side;

    return mapping;
}

static void _intmap_mapping_destroy(struct intmap_mapping *mapping) {
    /* _intmap_mapping_destroy
     * Destroy the supplied intmap mapping.
     *
     * @ struct intmap_mapping *mapping - The intmap mapping to destroy.
     */

    free(mapping);
}


static struct intmap_mapping_list *_intmap_mapping_list_create(intmap_int_t size) {
    /* _intmap_mapping_list_create
     * Create a new empty intmap mapping list with the supplied size.
     *
     * @ intmap_int_t size - The size of the mapping list.
     *
     * # struct intmap_mapping_list * - The newly created intmap mapping list.
     */

    struct intmap_mapping_list *mappings;

    mappings = malloc(sizeof(struct intmap_mapping_list));
    if(mappings == NULL) {
        die("_intmap_mapping_list_create() failed: malloc(%ld) failed: %s", sizeof(struct intmap_mapping_list),
                                                                            strerror(errno));
    }

    mappings->mappings = malloc(sizeof(struct intmap_mapping *) * size);
    if(mappings->mappings == NULL) {
        die("_intmap_mapping_list_create() failed: malloc(%ld) failed: %s", (sizeof(struct intmap_mapping *) * size),
                                                                            strerror(errno));
    }
    memset(mappings->mappings, 0, (sizeof(struct intmap_mapping *) * size));

    mappings->size = size;

    return mappings;
}

static void _intmap_mapping_list_destroy(struct intmap_mapping_list *mappings) {
    /* _intmap_mapping_list_destroy
     * Destroy the supplied intmap mapping list.
     *
     * @ struct intmap_mapping_list *mappings - The intmap mapping list to destroy.
     */

    intmap_int_t iter;
    struct intmap_mapping *mapping;

    for(iter = 0; iter < mappings->size; iter++) {
        mapping = mappings->mappings[iter];
        if(mapping != NULL) {
            _intmap_mapping_destroy(mapping);
        }
    }

    free(mappings->mappings);

    free(mappings);
}

static void _intmap_mapping_list_set(struct intmap_mapping_list *mappings,
                                     intmap_int_t index, struct intmap_mapping *mapping) {
    /* _intmap_mapping_list_set
     * Set the mapping at the the supplied into in the supplied intmap mapping list to the supplied value.
     *
     * @ struct intmap_mapping_list *mappings - The intmap mapping list to set the mapping in.
     * @ intmap_int_t index                   - The index in the mapping list to set the mapping at.
     * @ struct intmap_mapping *mapping       - The mapping to set.
     */

    mappings->mappings[index] = mapping;
}

static struct intmap_mapping *_intmap_mapping_list_get(struct intmap_mapping_list *mappings, intmap_int_t index) {
    /* _intmap_mapping_list_get
     * Get the mapping at the supplied index from the supplied intmap mapping list.
     *
     * @ struct intmap_mapping_list *mappings - The intmap mapping list to get the mapping from.
     * @ intmap_int_t index                   - The index in the mapping list to get the mapping from.
     *
     * # struct intmap_mapping * - The intmap mapping, or INTMAP_MAP_NONE if no mapping exists.
     */

    if(index >= mappings->size) {
        return INTMAP_MAP_NONE;
    }

    return mappings->mappings[index];
}


struct intmap *intmap_create(const struct intmap_mapping *static_mappings) {
    /* intmap_create
     * Create a new intmap from the supplied static mappings.
     * The static mappings must be a list of intmap_mapping structures, terminated by a INTMAP_MAP_END sentinel.
     *
     * @ const struct intmap_mapping *static_mappings - The static mappings to create the map from.
     *
     * # struct intmap * - The newly created intmap.
     */

    struct intmap *map;
    intmap_int_t iter, max_left_side, max_right_side;
    const struct intmap_mapping *static_mapping;
    struct intmap_mapping *mapping;

    map = malloc(sizeof(struct intmap));
    if(map == NULL) {
        die("intmap_create() failed: malloc(%ld) failed: %s", sizeof(struct intmap), strerror(errno));
    }


    /* Get max left/right side values. */
    max_left_side = 0;
    max_right_side = 0;
    for(iter = 0;; iter++) {
        if(iter >= UINTPTR_MAX) {
            die("intmap_create() failed: Attempted to iterate outside of valid range (missing INTMAP_MAP_END)");
        }

        static_mapping = &static_mappings[iter];
        if(static_mapping->left_side == INTMAP_MAP_END || static_mapping->right_side == INTMAP_MAP_END) {
            break;
        }

        if(static_mapping->left_side > max_left_side) {
            max_left_side = static_mapping->left_side;
        }

        if(static_mapping->right_side > max_right_side) {
            max_right_side = static_mapping->right_side;
        }
    }


    map->left_side_mappings  = _intmap_mapping_list_create((max_left_side + 1));
    map->right_side_mappings = _intmap_mapping_list_create((max_right_side + 1));


    /* Create mappings. */
    for(iter = 0;; iter++) {
        if(iter >= UINTPTR_MAX) {
            die("intmap_create() failed: Attempted to iterate outside of valid range (missing INTMAP_MAP_END)");
        }

        static_mapping = &static_mappings[iter];
        if(static_mapping->left_side == INTMAP_MAP_END || static_mapping->right_side == INTMAP_MAP_END) {
            break;
        }

        mapping = _intmap_mapping_create(static_mapping->left_side, static_mapping->right_side);
        _intmap_mapping_list_set(map->left_side_mappings, static_mapping->left_side, mapping);

        mapping = _intmap_mapping_create(static_mapping->left_side, static_mapping->right_side);
        _intmap_mapping_list_set(map->right_side_mappings, static_mapping->right_side, mapping);
    }

    return map;
}

void intmap_destroy(struct intmap *map) {
    /* intmap_destroy
     * Destroy the supplied intmap.
     *
     * @ struct intmap *map - The intmap to destroy.
     */

    _intmap_mapping_list_destroy(map->right_side_mappings);
    _intmap_mapping_list_destroy(map->left_side_mappings);

    free(map);
}


intmap_int_t intmap_right_from_left(struct intmap *map, intmap_int_t left_side) {
    /* intmap_right_from_left
     * Get the right side value for the supplied left side value in the supplied map.
     *
     * @ sturct intmap *map     - The map to use.
     * @ intmap_int_t left_side - The left side value to map to the right side.
     *
     * # intmap_int_t - The right side value, or INTMAP_MAP_END if there is no mapping for the left side value.
     */

    struct intmap_mapping *mapping;

    mapping = _intmap_mapping_list_get(map->left_side_mappings, left_side);
    if(mapping == INTMAP_MAP_NONE) {
        return INTMAP_MAP_END;
    }

    return mapping->right_side;
}

intmap_int_t intmap_left_from_right(struct intmap *map, intmap_int_t right_side) {
    /* intmap_left_from_right
     * Get the left side for the supplied right side value in the supplied map.
     *
     * @ struct intmap *map      - The map to use.
     * @ intmap_int_t right_side - The right side value to map to the left side.
     *
     * # intmap_int_t - The left side value, or INTMAP_MAP_END if there is no mapping for the right side value.
     */

    struct intmap_mapping *mapping;

    mapping = _intmap_mapping_list_get(map->right_side_mappings, right_side);
    if(mapping == INTMAP_MAP_NONE) {
        return INTMAP_MAP_END;
    }

    return mapping->left_side;
}
