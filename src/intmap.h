#ifndef INTMAP_H_INCLUDED
#define INTMAP_H_INCLUDED

#include <stdint.h>


/* Constants. */
#define INTMAP_MAP_END  UINTPTR_MAX
#define INTMAP_MAP_NONE NULL


/* Type definitions. */
typedef uintptr_t intmap_int_t;

struct intmap_mapping {
    intmap_int_t left_side, right_side;
};

struct intmap_mapping_list {
    struct intmap_mapping **mappings;
    intmap_int_t size;
};

struct intmap {
    struct intmap_mapping_list *left_side_mappings, *right_side_mappings;
};


/* Function prototypes. */
struct intmap *intmap_create(const struct intmap_mapping *static_mappings);
void intmap_destroy(struct intmap *map);

intmap_int_t intmap_right_from_left(struct intmap *map, intmap_int_t left_side);
intmap_int_t intmap_left_from_right(struct intmap *map, intmap_int_t right_side);

#endif
