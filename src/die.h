#ifndef DIE_H_INCLUDED
#define DIE_H_INCLUDED

/* Constants. */
#define DIE_MESSAGE_SIZE_MAX 2048


/* Macros. */
#define die(format, ...) die_internal(__FILE__, __LINE__, format, ##__VA_ARGS__)


/* Function prototypes. */
/* This function is internal, and should not be called directly, but needs to be public so the linker can
 * stil find it once the die() macro resolves into it.
 */
void die_internal(const char *file_name, int file_line, const char *format, ...);

#endif
