#ifndef INPUT_H_INCLUDED
#define INPUT_H_INCLUDED

#include <limits.h>

#include "intmap.h"


/* Constants. */
#define INPUT_ARROW_DOWN           110000
#define INPUT_ARROW_UP             110001
#define INPUT_ARROW_LEFT           110002
#define INPUT_ARROW_RIGHT          110003

#define INPUT_SHIFTED_ARROW_DOWN   120000
#define INPUT_SHIFTED_ARROW_UP     120001
#define INPUT_SHIFTED_ARROW_LEFT   120002
#define INPUT_SHIFTED_ARROW_RIGHT  120003

#define INPUT_BACKSPACE            130000
#define INPUT_PAGE_NEXT            130001
#define INPUT_PAGE_PREVIOUS        130002
#define INPUT_RETURN               130003
#define INPUT_HOME                 130004
#define INPUT_END                  130005
#define INPUT_DELETE               130006
#define INPUT_TAB                  130007

#define INPUT_SHIFTED_HOME         140000
#define INPUT_SHIFTED_END          140001
#define INPUT_SHIFTED_DELETE       140002
#define INPUT_SHIFTED_TAB          140003

#define INPUT_FUNCTION_0           150000
#define INPUT_FUNCTION_1           150001
#define INPUT_FUNCTION_2           150003
#define INPUT_FUNCTION_3           150004
#define INPUT_FUNCTION_4           150005
#define INPUT_FUNCTION_5           150006
#define INPUT_FUNCTION_6           150007
#define INPUT_FUNCTION_7           150008
#define INPUT_FUNCTION_8           150009
#define INPUT_FUNCTION_9           150010
#define INPUT_FUNCTION_10          150011
#define INPUT_FUNCTION_11          150012
#define INPUT_FUNCTION_12          150013
#define INPUT_FUNCTION_13          150014
#define INPUT_FUNCTION_14          150015
#define INPUT_FUNCTION_15          150016

#define INPUT_ESCAPE               160000
#define INPUT_PAUSE_BREAK          160001
#define INPUT_INSERT               160002

#define INPUT_CONTROL_A            170000
#define INPUT_CONTROL_B            170001
#define INPUT_CONTROL_C            170002
#define INPUT_CONTROL_D            170003
#define INPUT_CONTROL_E            170004
#define INPUT_CONTROL_F            170005
#define INPUT_CONTROL_G            170006
#define INPUT_CONTROL_H            170007
#define INPUT_CONTROL_I            170008
#define INPUT_CONTROL_J            170009
#define INPUT_CONTROL_K            170010
#define INPUT_CONTROL_L            170011
#define INPUT_CONTROL_M            170012
#define INPUT_CONTROL_N            170013
#define INPUT_CONTROL_O            170014
#define INPUT_CONTROL_P            170015
#define INPUT_CONTROL_Q            170016
#define INPUT_CONTROL_R            170017
#define INPUT_CONTROL_S            170018
#define INPUT_CONTROL_T            170019
#define INPUT_CONTROL_U            170020
#define INPUT_CONTROL_V            170021
#define INPUT_CONTROL_W            170022
#define INPUT_CONTROL_X            170023
#define INPUT_CONTROL_Y            170024
#define INPUT_CONTROL_Z            170025


/* Type definitions. */
typedef int input_t;


/* Global structures. */
struct input_context {
    struct intmap *input_character_map;
};


/* Function prototypes. */
void input_module_init(void);

input_t input_get(void);

#endif
