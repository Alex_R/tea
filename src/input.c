#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <curses.h>

#include "input.h"

#include "die.h"
#include "intmap.h"
#include "terminal.h"


/* Global structure initialisation. */
struct input_context _input_context = {
    .input_character_map     = NULL,
};


/* Input map definitions. */
static const struct intmap_mapping _input_character_map_mappings[] = {
    /* Control characters. */
    {1,             INPUT_CONTROL_A},
    {2,             INPUT_CONTROL_B},
    {3,             INPUT_CONTROL_C},
    {4,             INPUT_CONTROL_D},
    {5,             INPUT_CONTROL_E},
    {6,             INPUT_CONTROL_F},
    {7,             INPUT_CONTROL_G},
    /* 8 should be CONTROL_H, but is actually BACKSPACE (key code). */
    {9,             INPUT_TAB},       /* Also CONTROL_I. */
    {10,            INPUT_CONTROL_J},
    {11,            INPUT_CONTROL_K},
    {12,            INPUT_CONTROL_L},
    {13,            INPUT_RETURN},    /* Also CONTROL_M. */
    {14,            INPUT_CONTROL_N},
    {15,            INPUT_CONTROL_O},
    {16,            INPUT_CONTROL_P},
    {17,            INPUT_CONTROL_Q},
    {18,            INPUT_CONTROL_R},
    {19,            INPUT_CONTROL_S},
    {20,            INPUT_CONTROL_T},
    {21,            INPUT_CONTROL_U},
    {22,            INPUT_CONTROL_V},
    {23,            INPUT_CONTROL_W},
    {24,            INPUT_CONTROL_X},
    {25,            INPUT_CONTROL_Y},
    {26,            INPUT_CONTROL_Z},

    /* Arrow keys. */
    {KEY_DOWN,      INPUT_ARROW_DOWN},
    {KEY_UP,        INPUT_ARROW_UP},
    {KEY_LEFT,      INPUT_ARROW_LEFT},
    {KEY_RIGHT,     INPUT_ARROW_RIGHT},

    /* Shifted arrow keys. */
    {KEY_SF,        INPUT_SHIFTED_ARROW_DOWN},
    {KEY_SR,        INPUT_SHIFTED_ARROW_UP},
    {KEY_SLEFT,     INPUT_SHIFTED_ARROW_LEFT},
    {KEY_SRIGHT,    INPUT_SHIFTED_ARROW_RIGHT},

    /* Standard action keys. */
    {KEY_BACKSPACE, INPUT_BACKSPACE},
    {KEY_NPAGE,     INPUT_PAGE_NEXT},
    {KEY_PPAGE,     INPUT_PAGE_PREVIOUS},
    {KEY_ENTER,     INPUT_RETURN},
    {KEY_HOME,      INPUT_HOME},
    {KEY_END,       INPUT_END},
    {KEY_DC,        INPUT_DELETE},

    /* Shifted standard action keys (some aren't available). */
    {KEY_SHOME,     INPUT_SHIFTED_HOME},
    {KEY_SEND,      INPUT_SHIFTED_END},
    {KEY_SDC,       INPUT_SHIFTED_DELETE},

    /* Function keys. */
    {KEY_F(0),      INPUT_FUNCTION_0},
    {KEY_F(1),      INPUT_FUNCTION_1},
    {KEY_F(2),      INPUT_FUNCTION_2},
    {KEY_F(3),      INPUT_FUNCTION_3},
    {KEY_F(4),      INPUT_FUNCTION_4},
    {KEY_F(5),      INPUT_FUNCTION_5},
    {KEY_F(6),      INPUT_FUNCTION_6},
    {KEY_F(7),      INPUT_FUNCTION_7},
    {KEY_F(8),      INPUT_FUNCTION_8},
    {KEY_F(9),      INPUT_FUNCTION_9},
    {KEY_F(10),     INPUT_FUNCTION_10},
    {KEY_F(11),     INPUT_FUNCTION_11},
    {KEY_F(12),     INPUT_FUNCTION_12},
    {KEY_F(13),     INPUT_FUNCTION_13},
    {KEY_F(14),     INPUT_FUNCTION_14},
    {KEY_F(15),     INPUT_FUNCTION_15},

    /* Other action keys. */
    {27,            INPUT_ESCAPE},
    {KEY_BREAK,     INPUT_PAUSE_BREAK},
    {KEY_IC,        INPUT_INSERT},

    /* Other shifted keys (some aren't available). */
    {KEY_BTAB,      INPUT_SHIFTED_TAB},

    {INTMAP_MAP_END, INTMAP_MAP_END},
};


static void _input_module_cleanup(void) {
    /* _input_module_cleanup
     * Cleanup the input module.
     * Set as an exit callback by atexit() in input_module_init().
     */

    intmap_destroy(_input_context.input_character_map);
}

void input_module_init(void) {
    /* input_module_init
     * Initialise the input module.
     */

    _input_context.input_character_map = intmap_create(_input_character_map_mappings);

    if(atexit(_input_module_cleanup) != 0) {
        die("input_module_init() failed: atexit() failed: %s", strerror(errno));
    }
}


static input_t _input_map_from_character(int character) {
    /* _input_map_from_character
     * Map the supplied character to an input value.
     *
     * @ int character - The character value.
     *
     * # input_t - The input value.
     */

    /* Standard ascii characters. */
    if(character >= ' ' && character <= '~') {
        return (input_t) character;
    }

    return (input_t) intmap_right_from_left(_input_context.input_character_map, (intmap_int_t) character);
}


input_t input_get(void) {
    /* input_get
     * Get a single unit of input from the user.
     * The input will either be an ascii character, or a special INPUT_* value (see input.h for definitions).
     *
     * # input_t - The user's input.
     */

    int character;
    input_t input;

    while(1) {
        character = terminal_get_input();

        input = _input_map_from_character(character);
        if(input != INTMAP_MAP_END) {
            return input;
        }
    }
}
