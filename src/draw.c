#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "draw.h"

#include "die.h"
#include "config.h"
#include "terminal.h"
#include "editor.h"
#include "file.h"


struct draw_context *draw_context_create(void) {
    /* draw_context_create
     * Create a new draw context structure.
     * The context is used to track start/end points etc between draw calls.
     *
     * # struct draw_context * - The newly created draw context.
     */

    struct draw_context *context;

    context = malloc(sizeof(struct draw_context));
    if(context == NULL) {
        die("draw_context_create() failed: malloc(%ld) failed: %s", sizeof(struct draw_context), strerror(errno));
    }

    context->file_start = 0;
    context->file_end = 0;

    context->file_cursor_x = 0;
    context->file_cursor_y = 0;

    return context;
}

void draw_context_destroy(struct draw_context *context) {
    /* draw_context_destroy
     * Destroy the supplied draw context.
     *
     * @ struct draw_context *context - The draw context to destroy.
     */

    free(context);
}


void draw_context_file_start_set(struct draw_context *context, size_t file_start) {
    /* draw_context_file_start_set
     * Set the supplied draw context's file start position to the supplied file.
     *
     * @ struct draw_context *context - The draw context to set the file start position of.
     * @ size_t file_start            - The file to set the file start position to.
     */

    context->file_start = file_start;
}

size_t draw_context_file_start_get(struct draw_context *context) {
    /* draw_context_file_start_get
     * Get the supplied draw context's file start position.
     *
     * @ struct draw_context *context - The draw context to get the file start position of.
     *
     * # size_t - The draw context's file start position.
     */

    return context->file_start;
}


size_t draw_context_file_end_get(struct draw_context *context) {
    /* draw_context_file_end_get
     * Get the supplied draw context's file end position.
     *
     * @ struct draw_context *context - The draw context to get the file end position of.
     *
     * # size_t - The draw context's file end position.
     */

    return context->file_end;
}


static char _draw_file_get_drawable_byte(unsigned char file_byte) {
    /* _draw_file_get_drawable_byte
     * Map the supplied file byte into a "drawable" byte.
     * The file byte could be anything, and we don't want to start spraying terminal control characters around,
     * so we replace non-drawable characters with '?'.
     *
     * @ unsigned char file_byte - The file byte to map to a drawable byte.
     *
     * # char - The drawable byte.
     */

    /* Drawable asii characters range from 32 (space) to 126 (tilde), plus 10 (newline), and 9 (tab).
     */
    if((file_byte >= ' ' && file_byte <= '~') || (file_byte == '\n') || (file_byte == '\t')) {
        return (char) file_byte;
    }

    return '?';
}

static void _draw_file_eof_characters(int from_y) {
    /* _draw_file_eof_characters
     * Draw EOF characters from the supplied y coordinate to the end of the terminal.
     *
     * @ int from_y - The y coordinate to start drawing EOF tokens from.
     */

    int y;

    for(y = from_y; y < terminal_get_height(); y++) {
        terminal_cursor_move(0, y);
        terminal_draw_char_nomove('~');
    }
}

void draw_file(struct draw_context *context, struct file *file) {
    /* draw_file
     * Draw the supplied file, using the supplied draw context.
     *
     * @ struct draw_context *context - The context to use and update when drawing the file.
     * @ struct file *file            - The file to draw.
     */

    size_t file_position;
    unsigned char file_byte;
    char draw_byte;
    int term_cursor_x, term_cursor_y;

    term_cursor_x = 0;
    term_cursor_y = 0;

    for(file_position = context->file_start; file_position < file_data_size_get(file); file_position++) {
        if(file_position == file_cursor_get(file)) {
            context->file_cursor_x = term_cursor_x;
            context->file_cursor_y = term_cursor_y;
        }


        file_byte = file_byte_get(file, file_position);
        draw_byte = _draw_file_get_drawable_byte(file_byte);


        terminal_cursor_move(term_cursor_x, term_cursor_y);
        terminal_draw_char_nomove(draw_byte);


        if(draw_byte == '\t') {
            term_cursor_x += config_tab_size_get();
        } else {
            term_cursor_x++;
        }

        if(term_cursor_x >= terminal_get_width() || draw_byte == '\n') {
            term_cursor_x = 0;

            term_cursor_y++;
            if(term_cursor_y >= terminal_get_height()) {
                break;
            }
        }
    }

    context->file_end = file_position;

    if(term_cursor_y >= terminal_get_height()) {
        term_cursor_y = (terminal_get_height() - 1);
    } else {
        if(term_cursor_y < (terminal_get_height() - 1)) {
            /* If we've not drawn to the end of the terminal, increment the draw end position so that
             * it's always ahead of the cursor.
             */
            context->file_end++;
        }
    }

    /* Cursor at end of file. */
    if(file_position == file_cursor_get(file)) {
        context->file_cursor_x = term_cursor_x;
        context->file_cursor_y = term_cursor_y;
    }

    _draw_file_eof_characters(term_cursor_y + 1);

    terminal_cursor_move(context->file_cursor_x, context->file_cursor_y);
}

size_t draw_file_recalculate_end(struct draw_context *context, struct file *file, size_t file_start) {
    /* draw_file_recalculate_end
     * Recalculate the draw end based on the supplied file start position.
     *
     * @ struct draw_context *context - The context to use.
     * @ struct file *file            - The file to draw.
     * @ size_t file_start            - The file start position to use (instead of the one in the context).
     *
     * # size_t - The file draw end position.
     */

    size_t file_position;
    unsigned char file_byte;
    char draw_byte;
    int term_cursor_x, term_cursor_y;

    term_cursor_x = 0;
    term_cursor_y = 0;

    for(file_position = file_start; file_position < file_data_size_get(file); file_position++) {
        file_byte = file_byte_get(file, file_position);
        draw_byte = _draw_file_get_drawable_byte(file_byte);

        if(draw_byte == '\t') {
            term_cursor_x += config_tab_size_get();
        } else {
            term_cursor_x++;
        }

        if(term_cursor_x >= terminal_get_width() || draw_byte == '\n') {
            term_cursor_x = 0;

            term_cursor_y++;
            if(term_cursor_y >= terminal_get_height()) {
                break;
            }
        }
    }

    return file_position;
}


void draw_editor_message(struct draw_context *context, int type, const char *content) {
    /* draw_editor_message
     * Draw an editor message with the supplied type and content, within the supplied draw context.
     *
     * @ struct draw_context *context - The context to use and update when drawing the message.
     * @ int type                     - The type of the message (see EDITOR_MESSAGE_* constants).
     * @ const char *content          - The content of the message.
     */

    int colour_id;
    int iter;

    switch(type) {
        case EDITOR_MESSAGE_INFO:
            colour_id = TERMINAL_COLOUR_BLUE;
            break;
        case EDITOR_MESSAGE_SUCCESS:
            colour_id = TERMINAL_COLOUR_GREEN;
            break;
        case EDITOR_MESSAGE_WARN:
            colour_id = TERMINAL_COLOUR_YELLOW;
            break;
        case EDITOR_MESSAGE_ERROR:
            colour_id = TERMINAL_COLOUR_RED;
            break;

        default:
            colour_id = TERMINAL_COLOUR_WHITE;
    };

    terminal_cursor_move(0, (terminal_get_height() - 1));

    terminal_colour_set(colour_id);
    terminal_attr_set(TERMINAL_ATTR_HIGHLIGHT);

    terminal_draw_string_nomove(content);
    for(iter = strlen(content); iter < terminal_get_width(); iter++) {
        terminal_cursor_move(iter, (terminal_get_height() - 1));
        terminal_draw_char_nomove(' ');
    }

    terminal_attr_clear(TERMINAL_ATTR_HIGHLIGHT);
    terminal_colour_set(TERMINAL_COLOUR_WHITE);

    terminal_cursor_move(context->file_cursor_x, context->file_cursor_y);
}
