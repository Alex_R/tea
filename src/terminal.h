#ifndef TERMINAL_H_INCLUDED
#define TERMINAL_H_INCLUDED

#include <curses.h>


/* Constants. */
#define TERMINAL_ATTR_HIGHLIGHT A_STANDOUT
#define TERMINAL_ATTR_UNDERLINE A_UNDERLINE

#define TERMINAL_COLOUR_WHITE   1
#define TERMINAL_COLOUR_RED     2
#define TERMINAL_COLOUR_GREEN   3
#define TERMINAL_COLOUR_YELLOW  4
#define TERMINAL_COLOUR_BLUE    5
#define TERMINAL_COLOUR_MAGENTA 6
#define TERMINAL_COLOUR_CYAN    7


/* Global structures. */
struct terminal_context {
    WINDOW *window;
};


/* Function prototypes. */
void terminal_emergency_reset(void);
void terminal_module_init(void);

void terminal_get_width_height(int *width, int *height);
int terminal_get_width(void);
int terminal_get_height(void);

void terminal_tab_size_set(int tab_size);

void terminal_clear(void);
void terminal_refresh(void);

void terminal_cursor_move(int x, int y);

void terminal_draw_char(char character);
void terminal_draw_char_nomove(char character);

void terminal_draw_string(const char *string);
void terminal_draw_string_nomove(const char *string);

void terminal_attr_set(int attr_id);
void terminal_attr_clear(int attr_id);

void terminal_colour_set(int colour_id);

int terminal_get_input(void);

#endif
