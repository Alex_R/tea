tea - A text editor.

Copyright (c) Alex Richman <alex@richman.io>.

Released as free and open-source software under the ISC licence.


---

### Building
```
make
```

---

### Installing
```
make install # As root, if required.  Set PREFIX to change the install path.
```
