all:
	mkdir -p ./bin/

	clang -g -Wall -Werror \
	src/*.c -I src/ \
	`pkg-config --cflags --libs ncurses` \
	-o bin/tea


install:
	install -m 0755 ./bin/tea /${PREFIX}/usr/bin/


clean:
	rm -f ./bin/tea

	rmdir -f ./bin/
